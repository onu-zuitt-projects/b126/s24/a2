//Import Node's http moduleS
//let http = require("http");
//const index2 = require("./index2")

//index2.addOne()

//const jino = {
//	name: "Jino Yumul",
//	age: 32, 
//	occupation: "Instructor", 
//	faveColor: "Red", 
//	teach: function(){
//		console.log("Here's my lesson on Node.js")
//	}
//}

//jino.teach 

//function showName() {
//	console.log(jino.name)
//}

//call the function
//showName()

//Import Node's http module
//let http = require("http");
//const jino = require("./index2")

//Actually jino/index2 looks like this in this file when imported
/*
	const jino = {
		teach: function(){
			console.log("Here's my lesson on Node.js")
		}
	}

*/
//To call the function
//jino.teach()

//Import Node's http module
//let http = require("http");

/*
	In index2.js create 3 functions

	The first function will accept one number as its parameter and then print in the console the product of multiplying that number by itself

	The second function will accept one name as its parameter and then print in the console "Hello (name)"

	The third function will accept two parameters (one for each function) and then run the first and second functions again.

	In index.js, import and run each of these functions using node (node index in the console)

*/

//const index2 = require("./index2")

//index2.square(9)

//index2.sayHello("Jino")

//index2.repeatFunctions(3, "Paul")


//Import Node's http module
//the http module is a set of individual files that contain code to manage data transfers via a network connection
let http = require("http");

//The createServer() method lets you create an HTTP server that listens to requests on a specified port and can also send responses back to the client.
//A method is a function inside of an object

//Example: This is a function
//function myFunc(){
//	console.log("Hello")
//}

//This is a method
//let myObj = {
//	myMethod: function(){
//		console.log("Hello")
//	}
//}

//The server will be assigned to port 4000 via the listen() method where the server to listen to any requests that are sent to it when we try to communicate with that port.


const server = http.createServer(function (request, response){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	//writeHead allows sending an HTTP response header
	//HTTP request headers provided browsers with additional information about a website
	//included in the HTTP header is a response code, such as 200 - OK
	//there are also other codes like 404 - Not found and 505 - Internal Server Error

	response.end('Hello Obi') //the response to send when condition is met (user goes to localhost:4000) 

})

 const port = 4000 

server.listen(port) //if a user tries to communicatewith port 4000, send the response that we added in createServer()

console.log('Server running at port ${port}') //confirmation that server is running

//ports can be any number between 1024-65535
//ports 1 to 1023 are commonly-known as "Well-Known Ports" and should be kept reserved
//Well-Known Ports sjould always be kept open as they have specific and critical issues in
