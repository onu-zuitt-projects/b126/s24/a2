//Import Node's http module
//the http module is a set of individual files that contain code to manage data transfers via a network connection
let http = require("http");

const requestListener = function (request, response){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	//writeHead allows sending an HTTP response header
	//HTTP request headers provided browsers with additional information about a website
	//included in the HTTP header is a response code, such as 200 - OK
	//there are also other codes like 404 - Not found and 505 - Internal Server Error

	response.end('Hello Obi') //the response to send when condition is met (user goes to localhost:4000) 

}

const server = http.createServer(requestListener)

//The server will be assigned to port 4000 via the listen() method where the server to listen to any requests that are sent to it when we try to communicate with that port.
const port = 4000

console.log('Server running at port 4000')


Inside of routes.js, create an exportable function that will handle different endpoints for our server: 

module.exports.requestListener = function (request, response){
	response.writeHead(200, {'Content-Type': 'text/plain'})

	response.end('Hello Obi')

}

const server = http.createServer(requestListener)

//For the "/" endpoint, display a message saying "This is the homepage"

const server = http.createServer(function (request, response){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	//When a browser tries to contact a server, it sends a request
	//That request includes many different kinds of information
	//Among that information is the endpoint that the browser wants to access

	if(request.url === '/'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('This is the homepage')
	}else if(request.url === '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('This is the login page')
	}else if(request.url === '/about'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('This is the about page')		
	}else{
		response.writeHead(400, {'Content-Type': 'text/plain'})

		response.end('Page not found')		
	}

}) 
For the "/login" endpoint, display a message saying "This is the login page."

For the "/about" endpoint, display a message saying "This is the about page." 

For any other page, display a message saying "Page not found" with a 404 status code

Import that function into index.js and add it to the createServer() method 



const server = http.createServer(function (request, response){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	//When a browser tries to contact a server, it sends a request
	//That request includes many different kinds of information
	//Among that information is the endpoint that the browser wants to access

	if(request.url === '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Hello Obi')
	}else if(request.url === '/'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('This is the homepage')		
	}else{
		response.writeHead(400, {'Content-Type': 'text/plain'})

		response.end('Page not available')		
	}

})

 const port = 4000